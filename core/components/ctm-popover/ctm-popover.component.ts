import { Component, OnInit, Input } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-ctm-popover',
  templateUrl: './ctm-popover.component.html',
  styleUrls: ['./ctm-popover.component.scss'],
})
export class CtmPopoverComponent implements OnInit {
  @Input('options') options: any;

  constructor(private popoverController: PopoverController) { }

  ngOnInit() {}

  public dismissPopover() {
    this.popoverController.dismiss();
  }
}
