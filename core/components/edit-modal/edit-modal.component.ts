import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-edit-modal',
  templateUrl: './edit-modal.component.html',
  styleUrls: ['./edit-modal.component.scss'],
})
export class EditModalComponent implements OnInit {
  @Input('fields') fields: any;
  @Input('obj') obj: any;
  @Input('callback') callback: Function;

  constructor(private modalController: ModalController) { }

  ngOnInit() {
    this.obj = JSON.parse(JSON.stringify(this.obj));
  }

  public dismiss() {
    this.modalController.dismiss();
  }

  protected async getBase64FromImage(): Promise<string> {
    return new Promise(async (resolve, reject) => {
      var reader = new FileReader();
      const el: any = document.getElementById("image-input");
      const input = await el.getInputElement();
      if (!input || !input.files || !input.files.length) resolve(undefined);
      var f = input?.files;
  
      reader.onloadend = function () {
        const base64: any = reader.result;
        resolve(base64); 
      }
      reader.readAsDataURL(f[0]);
    });
  }

  public async applyChanges() {
    const requiredFields = this.fields.filter((f: any) => f.required);
    const imageFields = this.fields.filter((f: any) => f.type === 'image');
    let isValid = true;
    for (var i in imageFields) {
      this.obj[imageFields[i].name] = await this.getBase64FromImage() || undefined;
    }
    requiredFields.forEach((f: any) => {
      if (typeof this.obj[f.name] === 'undefined') { 
        isValid = false; 
      }
    })
    if (isValid) {
      this.callback(this.obj);
      this.dismiss();
    }
  }
}
